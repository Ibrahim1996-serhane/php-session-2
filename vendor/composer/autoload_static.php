<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit0951f95090de22606d73b4e5cc6c5038
{
    public static $files = array (
        '9b38cf48e83f5d8f60375221cd213eee' => __DIR__ . '/..' . '/phpstan/phpstan/bootstrap.php',
    );

    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/Phase2',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit0951f95090de22606d73b4e5cc6c5038::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit0951f95090de22606d73b4e5cc6c5038::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit0951f95090de22606d73b4e5cc6c5038::$classMap;

        }, null, ClassLoader::class);
    }
}
