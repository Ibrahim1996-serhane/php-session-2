<?php

namespace App\Classes;

use PDOException;

class User
{
    protected $conn;

    public function __construct()
    {
        $db = new Connection();
        $this->conn = $db->connect();
    }

    public function store(string $name, string $email)
    {
        try {
            $query = "INSERT INTO users (name, email) VALUES (:name, :email)";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':email', $email);

            $stmt->execute();
            echo "New record created successfully";

        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

}