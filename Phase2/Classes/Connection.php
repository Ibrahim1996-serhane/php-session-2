<?php

namespace App\Classes;

use PDO;
use PDOException;

class Connection
{
    /**
     * @return PDO|null
     */
    public function connect() : PDO|null
    {
        $servername = "localhost";
        $username = "khaldoun";
        $password = "root";

        try {
            $conn = new PDO("mysql:host=$servername;dbname=demoDB", $username, $password);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $conn;
        } catch(PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }

        return null;
    }

}